{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to successfully complete this assignment you need to turn in a project proposal to D2L on or before **11:59pm on Friday January 17**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center>  Modeling Interphase Convection Heat Transfer in a Packed Bed Reactor </center>\n",
    "\n",
    "<center>Keith King</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<img src=\"https://ars.els-cdn.com/content/image/1-s2.0-S0009250916306005-fx1.jpg\" width=\"35%\">\n",
    "<p style=\"text-align: right;\">Image from: https://www.sciencedirect.com/science/article/pii/S0009250916306005</p>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Overview\n",
    "\n",
    "Porous materials have many applications in science and engineering. They play important roles in oil exploration and production, packed beds in chemical reactors, biological applications, and many more areas of interest. As a result, it is important to be able to model their behaviors and properties in thermal-fluid engineernig applications. \n",
    "\n",
    "In my research, we are particularly interested in the heat transfer within a packed bed of pellets for a high-temperature thermochemical reactor. In particular, I need to measure the heat transfer coefficient (HTC) for the exchange of heat between the solid bed and the air flowing through the bed. The fundamental law governing this is Newton's Law of Cooling:\n",
    "\n",
    "                              Heat Transfer = Area*HTC*(T_hot-T_cold)\n",
    "                                      \n",
    "The most accurate way of determining the heat transfer coefficient is to model the heat transfer within an actively-fluidized packed bed using the Navier-Stokes and the Conservation of Energy equations. By applying volume-averaging theories to the equations derived at the microscopic level and scaling up to the macroscopic level, you can then model the two distinct phases of the porous material system (solid and fluid) as separate continua. This is critical because it allows us to discretize the domain with two overlapping continua, one solid and one fluid. We then write two separate energy equations to model the temperature of the solid and fluid phases individually. These two equations contain information about the percentage of the total system that is fluid and the remainder that is solid by incorporating the overall average porosity of the material into the equations, thus proportionalizing the resulting system to more accurately model the real thing. The convective heat exchanged between the phases emerges as a term in both energy equations as a part of the volume-averaging process and is represented mathematically by Newton's Law of Cooling.\n",
    "\n",
    "Ultimately, the heat transfer coefficient depends only on the fluid velocity and the geometry of the system. The goal of this type of continuum model is to determine the heat transfer coefficient for a given porous material geometry and fluid velocity. These data can then be used to create a correlation between fluid velocity-particle geometry and heat transfer within the bed to provide a general tool for design of packed-bed reactors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Program Description\n",
    "\n",
    "The program I will be building will be an Implicit Finite Difference numerical solver for solving a coupled system of partial differential equations. Finite difference equations are discretized algebraic expressions used to represent the value of a partial differential expression at a given point in space and time. These are approximations that allow for solving problems involving partial differential equations that cannot easily be solved analytically (if at all). An implicit algorthim requires that you solve each equation at every point in the model domain simultaneously. This approach is necessary because the solid and fluid phase energy equations are coupled, particularly in the boundary conditions, and thus cannot be solved explicitly. \n",
    "\n",
    "The following components already exist:\n",
    "1. Second order accurate finite difference expressions for the temperatures of both phases.\n",
    "2. An explicit numerical solver for the fluid velocity profile.\n",
    "3. Code for defining the system properties and geometries and for initializing the solution mesh.\n",
    "\n",
    "I need to create the following components:\n",
    "1. Code that implements an existing implict solution algorithm to solve the numerical equations.\n",
    "2. Code for varying the initial guess of the heat transfer coefficient and comparing temperature field results with experimental data to determine the correct HTC.\n",
    "3. Code for generating plots of the results.\n",
    "4. Higher-order accurate finite difference expressions to see if the accuracy of the solution is improved."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Project Goals and Timeline\n",
    "\n",
    "The specific parts of the code I am going to get working are as follows:\n",
    "\n",
    "1. A function for initializing the solution mesh grid based on an input of number of nodes.\n",
    "2. Individual functions for calculating each individual finite difference expression.\n",
    "3. A function that takes in properties and the results from the finite difference functions, uses the Thomas Algorithm to implicit solve my system of equations, then exports the temperature fields for each phase.\n",
    "4. A function that extracts the needed model temperatures and compares them to exisiting temperatures. If the accuracy is sufficient, it will export a signal to break the model and say that the model has finished. If the accuracy is not sufficient, it will update my heat transfer coefficient by some currently undefined amount, return to the beginning of the loop and begin again.\n",
    "5. Plotting functions to view my results.\n",
    "6. Possibly a larger loop that saves my result for a given average velocity, then reruns my program to solve for the HTC for a new set of experimental data. Basically this would just completely automate my workflow so that I don't have to run the model each time for new data and can just run it once and let it go til it's completely done.\n",
    "\n",
    "- Short Term Goals: Items 1, 2, and 4.\n",
    "- Medium Term Goals: Items 3 and 5 (3 will be the hardest part as it is the critical component of the model).\n",
    "- Long Term Goal: Item 6. This is not necessary and is just an aspirational goal if time allows.\n",
    "\n",
    "Initial Project Timeline:\n",
    "- 1/17/2020 - Proposal Due\n",
    "- 1/31/2020 - Project git repository and structure due\n",
    "- 2/14/2020 - Have functions written.\n",
    "- 2/28/2020 - Have Thomas Algorithm function written and working for simple matrix.\n",
    "- 3/1/2020 - Spring break week\n",
    "- 3/20/2020 - Have entire code written and incorporated together: functions, algorithm, plotting. Debugging not yet      required. (Code Review 1)\n",
    "- 4/3/2020 - Have code completely debugged and verify functionality with a known heat conduction problem. (Code Review 2) \n",
    "- 4/20/2020 - Final Project and Presentation Due\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Anticipating Challenges  \n",
    "\n",
    "Currently I think 3 main challenges will pop up:\n",
    "\n",
    "1. The Thomas Algorithim: this is an implicit algorithm that will work for my problem but that I as of yet do not fully understand. I need to study this a bit more and then try to get it working but could take some time to figure out properly. If it never works, I can probably find a different algorithm, but I don't expect this to be a problem.\n",
    "\n",
    "2. I'm simultaneously working on an experimental set-up for this project to collect the data. For the project to work as I've laid out, I'll need to have collected good data by April. This should be done, but I'm just listing this here because often problems pop up in experiments that are unexpected and set things back longer than planned. If this happens, I should be able to change my geometry and try to recreate the results of a paper my advisor wrote on a similar system years ago, so I have a backup plan just in case.\n",
    "\n",
    "3. Converting between MATLAB and Python: I will probably build my project in MATLAB as I go because it's the preferred language in my research lab. So, I'll build the components there, get them working, then convert them to Python. This isn't necessariy a challenge, but will just require some meticulous focus so that I don't mess up the indexing and so that I use all the right syntax needed in Python that isn't needed in MATLAB."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
