#Test functions for the porous medium heat transfer thermal field modeling software.
import pytest
import numpy as np
from porous_material_convection import porous_material_convection
      
def test_pellet_geometry_factors():
    #Make sure that the equations for the pellet geometry are calculating correct values.
    assert porous_material_convection.pellet_geometry_factors(1,1,0.5) == (0.001,3000.0)
    
    
def test_air_properties():
    # Call the air properties function.
    x,y = porous_material_convection.air_properties(25,1)
    # Check if values are correct.
    assert x > 1.18 and x < 1.19
    assert y > 1.8e-5 and y < 1.9e-5
    
def test_mean_velocity():
    #Make sure the velocity is being correctly calculated.
    assert porous_material_convection.mean_velocity(1,2,1,0.5) == 1
    
def test_assemble_linear_system():
    #Make sure that the system of equations is being assembled correctly.
    (A,RHS) = porous_material_convection.assemble_linear_system(1,0.1,10,10,0.5,10,1,10,2,1,25,10,10,1000) 
    assert A.shape == (200,200)
    assert RHS.shape == (200,1)
    assert np.sum(A) > 0
    assert np.sum(RHS) > 0  
