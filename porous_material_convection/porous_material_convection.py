#Porous Material Convection
'''This script contains the necessary functions for building a 2-D cylindrical coordinate
    steady-state temperature field solver for a fluidized packed bed. The solver works
    by generating a linear system of finite-difference equations for the two-equation,
    non-local thermal equilibrium model based on the system properties, assembling a
    single global system of equations, solving the system via matrix inversion, and
    finally extracting the temperature fields for the respective fluid and solid
    phases for subequent plotting and analysis.'''

import numpy as np

def pellet_geometry_factors(pellet_diameter, pellet_height, porosity):
    '''This function takes in the dimensions of an individual cylindrical pellet from the
    packed bed in millimeters and the bulk porosity of the bed. It calculates the hydraulic
    diameter of the pellet (a quantity from porous media fluid mechanics that is necessary
    for understanding the flow within a packed bed) and calculates the total surface area
    inside the bed through which heat can be exchanged between the solid and fluid phases.'''

    #Convert millimeter inputs to meters.
    radius = pellet_diameter/2/1000
    height = pellet_height/1000

    #Calculate the surface area and volume of an individual pellet.
    area_pellet = 2*np.pi*radius*height+2*np.pi*radius**2
    volume_pellet = np.pi*radius**2*height

    #Calculate the effective particle diameter of the cylindrical pellet.
    effective_particle_diameter = 6*volume_pellet/area_pellet

    #Calculate the interfacial area (i.e., total surface area between the solid
    #and fluid) within the packed bed based on the effective pellet diameter.
    surface_area = area_pellet/volume_pellet*(1-porosity)

    return effective_particle_diameter, surface_area


def air_properties(temp_inlet, p_mag):
    '''This function takes the temperature of the air at the inlet of the packed bed
    (in °C) and the magnitude of the air pressure at the inlet (i.e., how many times
    greater than/less than standard atmospheric pressure). From these
    conditions it calculates the density and viscosity (two key fluid properties) of
    the air in the system.'''

    #Update inlet temperature to be in Kelvin.
    temp_inlet_kelvin = temp_inlet + 273.15

    #Define the universal gas constant.
    gas_constant = 8.314 #J/kg*K

    #Define the molecular weight of air.
    molecular_weight_air = 28.97/1000 #kg/mol

    #Calculate the absolute pressure from the magnitude above or below atmospheric
    #pressure input to the function.
    p_absolute = p_mag*101325 #Absolute pressure in Pascals

    #Calculate the density of the air from the ideal gas law.
    density_air = molecular_weight_air*p_absolute/(gas_constant*temp_inlet_kelvin)

    #Calculate the viscosity of the air from a correlation from tabulated property data.
    viscosity_air = (1.993637734505*(10**-8)*(temp_inlet)**3 -
                     0.000034832356045969*(temp_inlet)**2 +
                     0.0490655774488893*(temp_inlet)+
                     17.1594385967813)*(10**-6)

    return density_air, viscosity_air

def mean_velocity(reynolds, density_air, viscosity_air, effective_particle_diameter):
    '''This function takes in the hydraulic-diameter-based Reynolds number
    (a dimensionless parameter commonly used in fluid mechanics), the
    hydraulic diameter, and the air properties and uses them to calculate
    the mean velocity of the air inside the packed bed.'''

    #Calculate the mean velocity within the packed bed from the Reynolds number and air properties.
    velocity = reynolds*viscosity_air/(density_air*effective_particle_diameter) #m/s

    return velocity




def assemble_linear_system(radius_i, radius_o, length, node_num, porosity,
                                       density_f, velocity, cond_s, cond_f,
                                       spec_heat_f, flux, temp_inlet, heat_transfer_coefficient,surface_area):
    '''This function takes in the dimensions of the packed bed and the number
    of nodes you want your solver grid to have. It assumes that the bed is an
    annulus with outer radius, inner radius, and bed length all defined in
    meters. It sets up your solution mesh grid, initializes all the arrays
    you'll need for organizing and manipulating the system of finite difference
    equations and those needed to store the final resulting temperature fields.
    Outputs are the following: the step sizes of the grid in both the radial
    and axial directions; empty arrays for the coefficient matrix and the
    solution vector for the global system of equations; and, empty arrays for
    storing the final temperature fields.


    This function also assembles the global coefficient matrix for the linear
    system of equations. The equations are the finite difference representation
    of the solid energy and fluid energy equations for a 2D system in
    cylindrical coordinates. All of the inputs are constants that show up in the
    equations or the boundary conditions, along with the grid structure, the
    empty storage arrays for the coefficient matrix and the solution vector, and
    the current guess for the heat transfer coefficient. This function also
    includes the boundary conditions at each of the four boundaries. Any
    changes to the definition of the BC's must be done within this function.
    It outputs the fully assembled global Linear system of equations utilizing
    a single global indexing scheme. A and RHS must be dimensionally compatible.'''
    
    #Convert inlet temperature to Kelvin from Celsius.
    temp_inlet = temp_inlet + 273.15
    
    #Multiply the heat transfer coefficient by the specific surface area to get it in volumetric form.
    heat_transfer_coefficient = heat_transfer_coefficient*surface_area

    #Calculate the bed height.
    height = (radius_o - radius_i)

    #Define the step-size for the grid mesh in each direction.
    delta_r = height/(node_num-1)
    delta_z = length/(node_num-1)

    #Initialize the arrays for the global variables.
    k_total = 2*node_num*node_num
    coeff_matrix = np.zeros([k_total, k_total])
    soln_vector = np.zeros([k_total, 1])

    #Define constants for easier coefficient matrix assembly.
    const_1 = (1-porosity)*cond_s
    const_2 = porosity*cond_f
    const_3 = 1/(delta_r**2)
    const_4 = 1/(delta_z**2)
    const_5 = 1/(2*delta_r)
    const_6 = density_f*spec_heat_f*velocity/(2*delta_z)
    node_num2 = node_num**2

    #Assemble the interior nodes of the global coefficient matrix.
    for i in range(1, node_num-1):
        for j in range(1, node_num-1):
            #Define k index.
            k_index = i*node_num+j
            #Populate the solid phase conduction portion of the matrix.
            coeff_matrix[k_index, k_index] = - 2*(const_3+const_4)*const_1-heat_transfer_coefficient
            coeff_matrix[k_index, k_index+node_num] = const_1*(const_3+const_5*
                                                               (1/(radius_i+(i)*delta_r)))
            coeff_matrix[k_index, k_index-node_num] = const_1*(const_3-const_5*
                                                               (1/(radius_i+(i)*delta_r)))
            coeff_matrix[k_index, k_index+1] = const_1*const_4
            coeff_matrix[k_index, k_index-1] = const_1*const_4
            #Populate the solid-equation convective exchange term portion of the matrix.
            coeff_matrix[k_index, k_index+node_num2] = heat_transfer_coefficient
            #Populate the fluid phase conduction and convective flow portion of the matrix.
            coeff_matrix[k_index+node_num2, k_index+node_num2] = -2*const_2*(const_3+const_4)-heat_transfer_coefficient
            coeff_matrix[k_index+node_num2, k_index+node_num+node_num2] = const_2*(const_3+const_5*(1/(radius_i+(i)*delta_r)))
            coeff_matrix[k_index+node_num2, k_index-node_num+node_num2] = const_2*(const_3-const_5*(1/(radius_i+(i)*delta_r)))
            coeff_matrix[k_index+node_num2, k_index+1+node_num2] = const_2*const_4-const_6
            coeff_matrix[k_index+node_num2, k_index-1+node_num2] = const_2*const_4+const_6

            #Populate the fluid-equation convective exchange term portion of the matrix.
            coeff_matrix[k_index+node_num2, k_index] = heat_transfer_coefficient

    #Apply the boundary conditions via global k-indexing.

    #Left of domain: inlet, z = 0. Constant inlet temp for fluid, derivative of temperature
    #gradient is 0 for the solid at the boundary.
    j = 0
    for i in range(0, node_num):
        #Define global k-index.
        k_index = i*node_num+j
        #Populate coefficient matrix for the solid BC.
        coeff_matrix[k_index, k_index] = 1
        coeff_matrix[k_index, k_index+1] = -2
        coeff_matrix[k_index, k_index+2] = 1
        #Populate coefficient matrix for the fluid BC.
        coeff_matrix[k_index+node_num2, k_index+node_num2] = 1
        #Specify the boundary value in the right-hand side vector.
        soln_vector[k_index] = 0
        soln_vector[k_index+node_num2] = temp_inlet

    #Right of domain: outlet, z = L. Derivative of temperature gradient is for both phases
    #at the boundary.

    j = node_num-1
    for i in range(0, node_num):
        #Define global k-index.
        k_index = i*node_num+j
        #Populate coefficient matrix for the solid BC.
        coeff_matrix[k_index, k_index] = 1
        coeff_matrix[k_index, k_index-1] = -2
        coeff_matrix[k_index, k_index-2] = 1
        #Populate coefficient matrix for the fluid BC.
        coeff_matrix[k_index+node_num2, k_index+node_num2] = 1
        coeff_matrix[k_index+node_num2, k_index-1+node_num2] = -2
        coeff_matrix[k_index+node_num2, k_index-2+node_num2] = 1
        #Specify the boundary value in the right-hand side vector.
        soln_vector[k_index] = 0
        soln_vector[k_index+node_num2] = 0

    #Bottom of domain: r = ri. Currently configured for constant heat flux.
    i = node_num-1
    for j in range(1, node_num-1):
        #Define global k-index.
        k_index = i*node_num+j

        #Populate coefficient matrix for the coupled--fluid-solid BC.
        coeff_matrix[k_index, k_index] = -3*(-(1-porosity)*cond_s/(2*delta_r))
        coeff_matrix[k_index, k_index-node_num] = 4*(-(1-porosity)*cond_s/(2*delta_r))
        coeff_matrix[k_index, k_index-2*node_num] = -1*(-(1-porosity)*cond_s/(2*delta_r))
        coeff_matrix[k_index, k_index+node_num2] = -3*(-porosity*cond_f/(2*delta_r))
        coeff_matrix[k_index, k_index-node_num+node_num2] = 4*(-porosity*cond_f/(2*delta_r))
        coeff_matrix[k_index, k_index-2*node_num+node_num2] = -1*(-porosity*cond_f/(2*delta_r))

        #Set local thermal equilibrium for fluid-solid temperature at wall.
        coeff_matrix[k_index+node_num2, k_index] = 1
        coeff_matrix[k_index+node_num2, k_index+node_num2] = -1

        #Specify the boundary value in the right-hand side vector.
        soln_vector[k_index] = flux
        soln_vector[k_index+node_num2] = 0

    #Top of domain: r = ro. Currently configured for adiabatic.
    i = 0
    for j in range(1, node_num-1):
        #Define global k-index.
        k_index = i*node_num+j

        #Populate coefficient matrix for the coupled-fluid-solid BC.
        coeff_matrix[k_index, k_index] = 3*((1-porosity)*cond_s/(2*delta_r))
        coeff_matrix[k_index, k_index+node_num] = -4*((1-porosity)*cond_s/(2*delta_r))
        coeff_matrix[k_index, k_index+2*node_num] = 1*((1-porosity)*cond_s/(2*delta_r))
        coeff_matrix[k_index, k_index+node_num2] = 3*(porosity*cond_f/(2*delta_r))
        coeff_matrix[k_index, k_index+node_num+node_num2] = -4*(porosity*cond_f/(2*delta_r))
        coeff_matrix[k_index, k_index+2*node_num+node_num2] = 1*(porosity*cond_f/(2*delta_r))

        #Define local thermal equilibrium at the wall between the two phases.
        coeff_matrix[k_index+node_num2, k_index] = 1
        coeff_matrix[k_index+node_num2, k_index+node_num2] = -1

        #Specify the boundary value in the right-hand side vector.
        soln_vector[k_index] = 0
        soln_vector[k_index+node_num2] = 0

    return coeff_matrix, soln_vector

def solve_linear_system(coeff_matrix, soln_vector):
    '''This function solves a system of linear equations by inverting the
    coefficient matrix and then multiplying the result by the source-vector
    to obtain a solution. A and RHS must be dimensionally compatible.'''

    temperature = np.linalg.solve(coeff_matrix,soln_vector)

    return temperature

def extract_temperature_fields(temperature, node_num):
    '''This function takes the result from the linear system solver and
    extracts the local temperature fields from the globally-indexed
    solution vector. It then converts the temperatures back to °C from
    Kelvin.'''

    #Initialize the arrays for the final temperature fields for the two phases.
    temp_s = np.zeros([node_num, node_num])
    temp_f = np.zeros([node_num, node_num])

    node_num2 = node_num**2

    #Loop through to extract all values.
    for i in range(0, node_num):
        for j in range(0, node_num):
            k_index = i*node_num+j
            temp_s[i, j] = temperature[k_index]
            temp_f[i, j] = temperature[k_index+node_num2]

    #Convert result to °Celsius.
    temp_s = temp_s - 273.15
    temp_f = temp_f - 273.15

    return temp_s, temp_f
