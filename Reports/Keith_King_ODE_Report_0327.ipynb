{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center>Using Ordinary Differential Equations (ODEs) in Porous Material Heat and Mass Transfer</center>\n",
    "\n",
    "<center>by Keith King</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "   The use of differential equations is widespread when studying energetic systems defined by a porous material. Heat and mass transfer are governed by a group of partial differential equations collectively known as the Navier-Stokes equations. These equations contain 3 main components[1]:\n",
    "   \n",
    "   1. Conservation of Mass: the mass that enters a system must be equal to the sum of the mass that leaves the system and any mass that is released or absorbed by a chemical reaction within the system.\n",
    "   2. Conservation of Momentum: any kinetic and potential energy contained within the system cannot be created or destroyed, and therefore must go somewhere within the system or be carried out of the system by the fluid moving through it.\n",
    "   3. Conservation of Energy: energy within the system cannot be created or destroyed and therefore is conserved.\n",
    "   \n",
    "This set of equations can be used to model any system in which energy is exchanged or mass is transported. The complexity of the equation depends entirely on the nature of the system: the simpler the system, the more terms can be neglected from the equations, thus making them simpler to solve using a variety of numerical methods or occassionally analytically. \n",
    "\n",
    "   In general, these equations usually remain as partial differential equations for real systems because they tend to be complex enough that not enough terms can be removed to convert to an ODE. However, there are two cases where simplfying to an ODE is actually quite useful. My current research can be used to provide a good example for both cases.\n",
    "   \n",
    "   The first case is when one needs to validate a numerical solver built to solve a complex system of PDE's. In these cases, one needs to test their model with some sort of known result to validate that the outputs of the model are accurate. Here is where ODE's come in: often, the ODE resulting from the heavily-simplified Navier-Stokes equations to model an idealized system will have an exact solution that can be analytically solved using calculus. I did just that in my research to validate that the numerical representatiosn of the energy equations for both the solid and fluid phases were behaving correctly within the solver. The full form of the equations for heat and mass transfer in porous materials can be found in Ref. [2]. For my system (a two-dimensional porous material with cylindrical geometry at steady-state), the energy equations reduce to the following:\n",
    "   \n",
    "   $$(1-\\epsilon)k_s(\\frac{\\partial ^2 T_s}{\\partial r^2}+\\frac{1}{r}\\frac{\\partial T_s}{\\partial r}+\\frac{\\partial ^2 T_s}{\\partial z^2})+h(T_f-T_s) = 0$$\n",
    "   $$\\rho_fc_pu\\frac{\\partial T_f}{\\partial z} = \\epsilon k_f(\\frac{\\partial ^2 T_f}{\\partial r^2}+\\frac{1}{r}\\frac{\\partial T_f}{\\partial r}+\\frac{\\partial ^2 T_f}{\\partial z^2})+h(T_s-T_f)$$\n",
    "   \n",
    "For solver validation, I examined four cases represented by four different ODE's with constant temperature BC's for which exact solutions could be found. In these cases, I adjusted the porosity and the fluid velocity such that I was examining stagnant conduction only in an individual phase to thus let me test each different component of the equations. I solved each equation analytically and then ran my model to numerically reproduce each case. If the numerical solution matched the exact solution, then the model was functioning properly. Those cases were the following:\n",
    "   1. Pure conduction in the solid in the axial (z) direction.\n",
    "    $$k_s(\\frac{\\partial ^2 T_s}{\\partial z^2}) = 0$$\n",
    "    <img src=\"Solid Axial Conduction Test 1_28_20.png\">\n",
    "   2. Pure conduction in the fluid in the axial (z) direction.\n",
    "   $$k_f(\\frac{\\partial ^2 T_f}{\\partial z^2}) = 0$$\n",
    "    <img src=\"Fluid Axial Conduction Test 1_28_20.png\">\n",
    "   3. Pure conduction in the solid in the radial (r) direction.\n",
    "   $$k_s(\\frac{\\partial ^2 T_s}{\\partial r^2}+\\frac{1}{r}\\frac{\\partial T_s}{\\partial r}) = 0$$\n",
    "    <img src=\"Solid Radial Conduction Test 1_28_20.png\">\n",
    "   4. Pure conduction in the fluid in the radial (r) direction.\n",
    "   $$k_f(\\frac{\\partial ^2 T_f}{\\partial r^2}+\\frac{1}{r}\\frac{\\partial T_f}{\\partial r}) = 0$$\n",
    "    <img src=\"Solid Radial Conduction Test 1_28_20.png\">\n",
    "\n",
    "As you can see, the model matched the exact ODE quite nicely, proving this to be a useful application of ODE's in my research. The second instance shows itself in the momentum equation. For my particular system, the momentum equation for fluid flow in a porous material reduces to the following equation:\n",
    "   \n",
    "   $$\\frac{-dP}{dz}-\\frac{\\mu}{K}u+\\frac{\\mu}{\\epsilon}(\\frac{d^2 u}{dr^2}+\\frac{1}{r}\\frac{du}{dr})-\\frac{\\rho_fc_f}{K^\\frac{1}{2}}u^2 = 0$$\n",
    "   \n",
    "As you can see, this equation is a second-order ODE. Because of the non-linearity in the u-squared term, this equation is extremely difficult to solve analytically. However, it can quite easily be solved numerically using an ODE-solver built into Python (such as was done in class) by redefining the equation as a system of two coupled first-order ODE's given as,\n",
    "   \n",
    "   $$u_1 = \\frac{du}{dr}$$\n",
    "   $$u_2 = \\frac{du_1}{dr}$$\n",
    "   \n",
    "and then substituting the above system definitions in to the momentum equation. By doing this, I can remove the non-linearity from the system and use the convenient built-in ODE solver to calculate the velocity profile within my system and thus extract the mean velocity that is fed into the energy equations shown above. These are just a few examples of the use of ODE's in heat and mass transfer within a porous material. In conclusion, ODE's are a wide-spread and absolutely critical modeling technique for modeling heat and mass transfer, not just in porous materials, but in all conceivable systems that the world has to offer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# References\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. Panton, R. L. (2013). Incompressible flow. John Wiley & Sons.\n",
    "2. Amhalhel, G., & Furmański, P. (1997). Problems of modeling flow and heat transfer in porous media. Journal of Power Technologies, 85, 55-88."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
